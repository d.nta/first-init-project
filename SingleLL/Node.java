package SingleLL;
public class Node<K> {
    private K data;
    private Node<K> next;
    Node(){
        this.data = null;
        this.next = null;
    }
    Node<K> getNext(){
        return this.next;
    }
    Node<K> setNext(Node<K> payload){
        this.next = payload;
        return this.next;
    }
    K getData(){
        return this.data;
    }
    K setData(K payload){
        this.data = payload;
        return this.data;
    }
}
